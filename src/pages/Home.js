import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

import {indigo400} from 'material-ui/styles/colors';

import Title from '../components/Title';
import Benefits from './../components/Benefits';
import PlaceCard from './../components/places/PlaceCard';
import data from '../requests/places';

import { TransitionGroup } from 'react-transition-group';
import Container from '../components/Container';
import { Link } from 'react-router-dom';

export default class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      places: []
    }

    setTimeout(() => this.setState({ places: data.places }), 2000)

    this.hidePlace = this.hidePlace.bind(this);
  }

  places () {
    return this.state.places.map((place, index) => {
      return(
        <PlaceCard onRemove={this.hidePlace} place={place} index={index} key={index} />
      );
    });
  }

  hidePlace(place) {
    this.setState({
      places: this.state.places.filter(el => el.id !== place.id)
    })
  }

  render() {
    return(
      <section>
        <div className="Header-background">
          <Container>
            <div className="Header-main">
              <Title />
              <Link to="/login">
                <RaisedButton label="Crear cuenta gratuita" secondary={true} />
              </Link>
              <img className="Header-illustration" src={process.env.PUBLIC_URL + '/images/place.png'} alt="Places" />
            </div>
            <div>
              <Benefits />
            </div>
          </Container>
        </div>
        <div style={{ 'backgroundColor': indigo400, 'padding': '50px', 'color': 'white' }}>
          <h3 style={{ 'fontSize': '2rem' }}>Sitios Populares</h3>
          <TransitionGroup
            className="row">
            { this.places() }
          </TransitionGroup>
        </div>
      </section>
    );
  };
}