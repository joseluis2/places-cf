import React from 'react';
import Container from '../components/Container';
import { FlatButton } from 'material-ui';
import data from '../requests/places';
import PlaceHorizontal from '../components/places/PlaceHorizontal';
import { Link } from 'react-router-dom';
import { FloatingActionButton } from 'material-ui';
import ContentAdd from 'material-ui/svg-icons/content/add';

export default class Dashboard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      places: data.places
    }
  }

  places() {
    return this.state.places.map((place, index) => {
      return <PlaceHorizontal place={place} key={index} />
    });
  }

  render() {
    return(
      <div>
        <Link to='/new'>
          <FloatingActionButton className="Fab" secondary={ true }>
            <ContentAdd />
          </FloatingActionButton>
        </Link>
        <Container>
          <div className="row">
            <div className="col-xs-12 col-md-2" style={{ 'textAlign': 'left' }}>
              <FlatButton label="Explorar" fullWidth={true} />
              <FlatButton label="Favoritos" fullWidth={true} />
              <FlatButton label="Visitas Previas" fullWidth={true} />
            </div>
            <div className="col-xs-12 col-md-10">
              {this.places()}
            </div>
          </div>
        </Container>
      </div>
    );
  }
}
