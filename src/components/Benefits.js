import React from 'react';
import { Card, CardText } from 'material-ui/Card';

import {redA400, lightBlueA400, amberA400} from 'material-ui/styles/colors';

export default class Benefit extends React.Component {
    render() {
        return(
            <ul className="Header-Benefit-list">
                <Card className="Header-Benefit">
                    <CardText className="Header-Benefit-item">
                    <div className="Header-Benefit-image" style={{'backgroundColor': redA400}}>
                        <img src={process.env.PUBLIC_URL + '/images/place.png'} alt="Calificaciones con emociones" />
                    </div>
                    <div className="Header-Benefit-content">
                        <h3>Calificaciones con emociones</h3>
                        <p>Califica tus lugares con experiencias, no con números</p>
                    </div>
                    </CardText>
                </Card>
                <Card className="Header-Benefit">
                    <CardText className="Header-Benefit-item">
                    <div className="Header-Benefit-image" style={{'backgroundColor': lightBlueA400}}>
                        <img src={process.env.PUBLIC_URL + '/images/place.png'} alt="Sin internet" />
                    </div>
                    <div className="Header-Benefit-content">
                        <h3>¿Sin internet? Sin problemas</h3>
                        <p>Placess funciona sin internet o en conexiones lentas</p>
                    </div>
                    </CardText>
                </Card>
                <Card className="Header-Benefit">
                    <CardText className="Header-Benefit-item">
                    <div className="Header-Benefit-image" style={{'backgroundColor': amberA400}}>
                        <img src={process.env.PUBLIC_URL + '/images/place.png'} alt="Tus lugares favoritos" />
                    </div>
                    <div className="Header-Benefit-content">
                        <h3>Tus lugares favoritos</h3>
                        <p>Define tu lista de lugares favoritos</p>
                    </div>
                    </CardText>
                </Card>
            </ul>
        );
    }
}