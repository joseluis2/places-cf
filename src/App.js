import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import MyAppBar from './components/navigation/MyAppBar';

import './App.css';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import { withRouter } from 'react-router-dom';

class App extends Component {

  render() {
    return (
        <MuiThemeProvider>
          <>
            <MyAppBar />
            <TransitionGroup>
              <CSSTransition classNames="left-out" timeout={300} key={this.props.location.pathname.split('/')[1]}>
                {this.props.children}
              </CSSTransition>
            </TransitionGroup>
          </>
        </MuiThemeProvider>
    );
  }
}

export default withRouter(App);
